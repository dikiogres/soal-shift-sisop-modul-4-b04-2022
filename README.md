# Kelompok B04 Sistem Operasi 2022

### Anggota

| Nama                               | NRP        |
|------------------------------------|------------|
| Lia Kharisma Putri                 | 5025201034 |
| Muhammad Dzikri Fakhrizal Syairozi | 5025201201 |
| Mohamad Kholid Bughowi             | 5025201253 |
<br/>

# Penjelasan soal-shift-sisop-modul-4-B04-2022

## Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
### Poin A
Semua direktori dengan awalan `“Animeku_”` akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan `atbash cipher` dan huruf kecil akan terencode dengan `rot13`
Contoh : 
`“Animeku_/anya_FORGER.txt”` → `“Animeku_/naln_ULITVI.txt”`

Berikut merupakan potongan kode untuk encode dengan atbash cipher dan rot13
```c
void atbashRot13(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    char ext[10], newFile[10000];
    memset(ext, 0, sizeof(ext));
    memset(newFile, 0, sizeof(newFile));
    int nExt = 0, nFile = 0, flag = 0;
    // get file name and ext
    for (int i = 0; i < strlen(file); i++)
    {
        if (file[i] == '.')
        {
            newFile[nFile++] = file[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
            ext[nExt++] = file[i];
        else
            newFile[nFile++] = file[i];
    }
    // encode or decode file name
    for (int i = 0; i < nFile; i++)
    {
        if (newFile[i] >= 65 && newFile[i] <= 90)
        {
            newFile[i] = 'A' + 'Z' - newFile[i];
        }
        else if (newFile[i] >= 97 && newFile[i] <= 122)
        {
            if (newFile[i] >= 97 && newFile[i] <= 109)
                newFile[i] += 13;
            else
                newFile[i] -= 13;
        }
    }
    // combine file name and ext
    strcat(newFile, ext);
    strcpy(file, newFile);
    printf("rot %s\n", file);
}
```

### Poin B
Semua direktori di-rename dengan awalan `“Animeku_”`, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
### Poin C
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
### Poin D
Setiap data yang terencode akan masuk dalam file `“Wibu.log”` 
Contoh isi: 
```
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
```
Berikut potongan kode untuk membuat file Wibu.log
```c
void writeLog1(char *func, char *type, const char *prevPath, const char *newPath)
{
    FILE *logFile = fopen("/home/bughowi/Wibu.log", "a");
    fprintf(logFile, "%s\t%s\t%s\t-->\t%s\n", func, type, prevPath, newPath);
    fclose(logFile);
}
```
### Poin E
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)


## Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :
### Poin A
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
Berikut deklarasi variabel key yang berisi `INNUGANTENG`
```c
char key[12] = "INNUGANTENG";
```
### Poin B
Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
Berikut potongan kode untuk encode dengan viginere
```c
void encodeVigenere(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    int i, j = 0;
    // encode file
    for (i = 0; i < strlen(file); i++)
    {
        if (key[j] == '\0')
        {
            j = 0;
        }
        if (file[i] >= 65 && file[i] <= 90)
        {
            file[i] = (file[i] - 'A' + key[j++] - 'A') % 26 + 'A';
        }
        else if (file[i] >= 97 && file[i] <= 122)
        {
            file[i] = (file[i] - 'a' + tolower(key[j++]) - 'a') % 26 + 'a';
        }
        else
        {
            j = 0;
        }
    }
}
```
### Poin C
Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
Berikut potongan kode untuk decode dengan viginere
```c
void decodeVigenere(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    int i, j = 0;
    // decode file
    for (i = 0; i < strlen(file); i++)
    {
        if (key[j] == '\0')
        {
            j = 0;
        }
        if (file[i] >= 65 && file[i] <= 90)
        {
            file[i] = (file[i] - 'A' - key[j++] + 'A' + 26) % 26 + 'A';
        }
        else if (file[i] >= 97 && file[i] <= 122)
        {
            file[i] = (file[i] - 'a' - tolower(key[j++]) + 'a' + 26) % 26 + 'a';
        }
        else
        {
            j = 0;
        }
    }
}
```
### Poin D
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
Berikut potongan kode untuk membuat file log yang disimpan di `home/bughowi/hayolongapain_b04.log`
```c
void writeLog2(char *func, const char *prev, const char *new)
{
    time_t curTime;
    struct tm *time_info;
    time(&curTime);
    time_info = localtime(&curTime);
    int year = time_info->tm_year + 1900;
    int month = time_info->tm_mon + 1;
    int day = time_info->tm_mday;
    int hour = time_info->tm_hour;
    int min = time_info->tm_min;
    int sec = time_info->tm_sec;
    FILE *logFile;
    logFile = fopen("/home/bughowi/hayolongapain_B04.log", "a");
    char level[10];
    char use[3] = "";
    if (strcmp(new, "") != 0)
        strcpy(use, "::");
    if (strcmp(func, "RMDIR") == 0 || strcmp(func, "UNLINK") == 0)
        strcpy(level, "WARNING");
    else
        strcpy(level, "INFO");
    fprintf(logFile, "%s::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s%s\n", level, day, month, year, hour, min, sec, func, prev, use, new);
    fclose(logFile);
    return;
}
```
### Poin E
Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut :
`[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]`

## Soal 3
